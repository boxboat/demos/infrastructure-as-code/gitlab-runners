# GitLab Runners
An IaC project that creates EC2 instances using Terrafrom and installs and registers a GitLab runner on the created machine. Can be run locally or via GitLab CI. Terraform state stored in the repository.
