
data "aws_vpcs" "target_vpc" {
  tags = {
    owner = var.owner_tag
    public = var.public_tag
  }
}

data "aws_subnet_ids" "target_subnets" {
  vpc_id = tolist(data.aws_vpcs.target_vpc.ids)[0]
  tags = {
    owner = var.owner_tag
    public = var.public_tag
  }
}

data "aws_ami" "ami_id" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

