terraform {
  # This module is now only being tested with Terraform 0.13.x. However, to make upgrading easier, we are setting
  # 0.12.26 as the minimum version, as that version added support for required_providers with source URLs, making it
  # forwards compatible with 0.13.x code.
  required_version = ">= 0.12.26"
  # https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#get-started-using-local-development
  backend "http" {
  }
}

provider "aws" {

  # https://support.hashicorp.com/hc/en-us/articles/360041289933-Using-AWS-AssumeRole-with-the-AWS-Terraform-Provider
  assume_role {
    # The role ARN within Account B to AssumeRole into. Created in step 1.
    role_arn    = var.assume_role_arn
    # (Optional) The external ID created in step 1c.
    # external_id = "my_external_id"
  }
}

resource "aws_key_pair" "ssh_key" {
  key_name   = "${var.prefix}-ssh-key"
  public_key = file(var.ssh_public_key_file)
}

resource "aws_instance" "gitlab" {
  count = var.instance_count

  instance_type = var.instance_size
  ami = coalesce(var.ami_id, data.aws_ami.ami_id.id)
  key_name = aws_key_pair.ssh_key.key_name
  subnet_id = tolist(data.aws_subnet_ids.target_subnets.ids)[0]

  associate_public_ip_address = var.public_tag

  vpc_security_group_ids = [
    aws_security_group.gitlab_external.id
  ]
  root_block_device {
    volume_size = 60
  }

  tags = {
    Name = "${var.prefix}-instance"
    owner = "jesse@boxboat.com"
    public = var.public_tag

    gitlab_node_prefix = var.prefix
    gitlab_node_type = "server"
    gitlab_node = true
  }

}
