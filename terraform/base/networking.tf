# resource  "aws_route53_record" "www" {
#   zone_id = var.hosted_zone_id
#   name    = "gitlab-dev.jesse.boxboat.io"
#   type    = "A"
#   ttl     = "60"
#   records = [aws_instance.gitlab.public_ip]
# }

resource "aws_security_group" "gitlab_external" {
  name = "${var.prefix}-external"

  description = "Allow GitLab Traffic"

  vpc_id      = tolist(data.aws_vpcs.target_vpc.ids)[0]

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2222
    to_port     = 2222
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
