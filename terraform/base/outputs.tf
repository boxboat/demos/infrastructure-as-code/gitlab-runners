output "target_subnets" {
  value = data.aws_subnet_ids.target_subnets.ids
}

output "target_subnet" {
  value = tolist(data.aws_subnet_ids.target_subnets.ids)[0]
}

output "vpc" {
  value = data.aws_vpcs.target_vpc.ids
}
