
variable "owner_tag" {
  default = "jesse@boxboat.com"
}

variable "instance_count" {
  default = "1"
}
variable "public_tag" {
  default = true
}

variable "instance_size" {
  default = "t3.medium"
  #default = "m5.large"
}

variable "hosted_zone_id" {
  default = "Z01280702B9XC5L04HI12"
}

variable "prefix" {
  default = "jca-webinar-dev"
}

variable "region" {
  default = "us-east-1"
}

variable "ssh_public_key_file" {
  default = "../../keys/dev_key.pub"
}

variable "assume_role_arn" {
  default = "arn:aws:iam::564087180491:role/Admin"
}

variable "ami_id" { default = null } # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami
